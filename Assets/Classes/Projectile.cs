﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody rb;
    public float force;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    public void Fire()
    {
        rb.AddForce(transform.forward * force);
    }
}
